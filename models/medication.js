/**
 * Author: Stephanie Miranda
 * Date: 12-19-2020
 * <version:1 className="0">1.0</version:1>
 * Description: A Mongoose model. The model is exported to GraphQL. We use a plugin to help validate BSON integer numbers. Note that a Mongoose model is a schema constructor, and an instance of a Mongoos model is a document in MongoDB. Models in Mongoose are responsible for creating and reading documents with the connect MongoDB. (Recall: MongoDB is not a relational DB, and that documents are akin to objects and collections are akin to tables.). Unique IDs created using nanoid, https://github.com/ai/nanoid. Integer validator plugin from https://www.npmjs.com/package/mongoose-integer.
 */

import { interfaceDeclaration } from 'babel-types';
import mongoose from 'mongoose';
import integerValidator from 'mongoose-integer';
import { nanoid } from 'nanoid';

/* Create a schema object for GraphQL, query data  is validated against this schema.*/
const Schema = mongoose.Schema;

/* This is the definition of the types of data we can query from the database. These definitions will apply to objects from the database, and define whether that data can be null, the type of data we expect to be returned, etc..*/
const MedSchema = new Schema({
     _id: {
          type: String,
          default: () => nanoid(),
          required: true,
          unique: true
     },
     name :{
          brand_name : {
               type: String,
               required: false
          },
          generic_name : {
               type: String,
               required: false
          },
          label_name : {
               type: String,
               required: true
          }
     },
     strength : {
          type: Number,
          integer: true,
          required: true
     },
     measure: {
          type: String,
          required: true
     },
     frequency: {
          type: Number,
          integer: true,
          required: true
     },
     total: {
          pills: {
               type: Number,
               required: true
          }
     },
     tablet: {
          type: String,
          required: false
     },
     capsule: {
          type: String,
          required: false
     },
     oral_liquid: {
          type: String,
          required: false
     },
     inject_liquid: {
          type: String,
          required: false
     },
     drop_liquid: {
          type: String,
          required: false
     },
     inhalent: {
          type: String,
          required: false
     },
     topical: {
          type: String,
          required: false
     }
});

/* Assign MedSchema to a variable for easier export. */
//var Medication = mongoose.model('Medication', MedSchema);

const UserSchema = new Schema({
     //user info
     _id: {
          type: String,
          default: () => nanoid(),
          required: true,
          unique: true
     },
     email: {
          type: String,
          required: true,
          unique: true
     },
     name: {
          first: {
               type: String,
               required: true
          },
          last: {
               type: String,
               required: true
          },
          user: {
               type: String,
               required: true,
               unique:  true
          }
     },
     //type could be patient, parent, caregiver
     user_type: {
          type:String,
          required: false
     }
});

/* Assign UserSchema to a variable for easier export. */
//var User = mongoose.model('User', UserSchema);

const PatientSchema = new Schema({
     //patient info
     _id: { 
          type: String,
          default: () => nanoid(),
          required: true,
          unique: true
     },
     patient_type: {
          user: {
               type: String,
               required:false
          },
          patient: {   
               type: String,
               required:false
          }
     },
     patient_name: {
          type: String,
          required: false
     }
});

/* Assign PatientSchema to a variable for easier export. */
//var Patient = mongoose.model('Patient', PatientSchema);

/* Run number types throught the plugin. */
MedSchema.plugin(integerValidator);

/* Export the model for use. Don't forget, methods and schema must be defined before compiling model(). */
/*module.exports = {
     Medication: Medication,
     User: User,
     Patient: Patient
};*/