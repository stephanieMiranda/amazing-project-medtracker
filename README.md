# MedTracker Web Application #

The purpose of this application is to help patients track their medications by dynamically creating tables based on medication instructions on a bottle. These tables will include a column for check boxes, so patients can easily check off they've taken the medication, a column each for time and date, and finally a column for patients to makes notes as needed. Those notes can be about anything, what meal they ate, any side effects they may be experiencing, if they forgot a medication or any other things.

If you'd like to skip the technical information about this application and get to using it on your machine, click [here](#installation) to get started.

#### _This application was derived from Yomi's two part tutorial posted on [Medium - Part 1](https://blog.bitsrc.io/how-to-build-a-note-taking-app-with-graphql-and-react-part-1-of-2-febf1aeda091 "Part 1") and [Medium - Part 2](https://blog.bitsrc.io/how-to-build-a-note-taking-app-with-graphql-and-react-part-2-of-2-b1f4f40361a)._ ####

### HOW IT WORKS ###
#### v1.0 ####

Patients will be prompted to login or create an account, after which they'll be redirected to a home dashboard. This dashboard will automatically display the most recent medication track table and include options to create new tables or view a list of all tables on their account.

The tables will be dynamic React views/components, and users will be able to click on arrows, right and left of the table, to access other tables in chronological order. This will help users who are possibly taking multiple medications at once. It will run on a simple local Express server, and Mongoose to connect this Express server to MongoDB and a GraphQL Server to send data back and forth from the frontend to the backend.

Tables are created based on information the patients input when they choose to create a table. The React create view will contain a form, in which the patient is prompted to fill in the name of the drug, the strength, the start date, the frequencey (for example the _number of tablets_ every _x_ hours), the number of tablets :pill:  or ounces (oz) of liquid :syringe:, and the instructions listed on the bottle. The application will then take this information and create a table with slots for each dose the patient is to take with the date and times listed. So, all the patient will see in the table are check boxes, time, date, and the column for notes.

If the user chooses to view the master list of tables, the view will show them a list of drug names, strengths and start/end dates in descending chronological order. The user can then simply scroll the throught the list to view that table in the dashboard, go back without changing the currently viewed table, or export a CSV file containing this information, which will be useful in taking to doctor's appointments.

### TOOLS ###
#### v1.0 ####

This application will make use of GraphQL to query the API, MongoDB 4.4 to manage database information, React to deliver the frontend components, and Apollo to connect the React frontend to the GraphQL API. Also, Okta will be used to securely create and login users. 

This application, for the time being, can be executed on a local machine with some simple NPM commands. To install please find instructions [here](#installation).

### [Installation](#installation) ###
#### v1.0 ####

## Start the Express server. ##

1. Open a Terminal on MacOS or Command Line Interface (CLI) on other operating systems. (from here on out this document will refer to this as the terminal)
2. Navigate to the medTracker-api folder.
3. Type the following command into the Terminal:
     `npm start`
4. In a web browser of your choosing, navigate to:
     <http://localhost:3250>

You'll notice in your terminal a note about what port this application is listening on adn how to stop the server easily in that terminal window.


## Stop the Express server. ##

1. In the same terminal you started the Express server in, on Mac and Windows type in, `Control + C`.


### THE END GOAL ###
#### v1.0 ####

Ultimately, I'd like to get this app hosted on AWS or something similar. It would be great if the app could function freely on the web without the need for installation of anything, or possibly adapt it to mobile applications to function on both Android and iOS.

It would also be useful to have the ability to make groups of tables for various house members or clients, with one or two managing house admins. This would allow for parents to track multiple kids' medications, pet medications, etc. or to allow caregivers to track multiple patients.

Finally, it would be great to connect this application to a reputable drug API so that as users are creating a table for a medication, the name can be autopopulated with brand or genericd and choose dosages/strengths from a dropdown. This version will heavily rely on the patient to properly input the data at the creation of a table.