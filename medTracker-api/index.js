/**
 * Author: Stephanie Miranda
 * Date: 12-17-2020
 * <version:1 className="0">1.0</version:1>
 * Desctiption:This file contains code to create and Express server for the MedTracker Web Application.
 */

import express from "express";
import mongoose from "mongoose";

/*Link API to MongoDB with Mongoose. UnifiedTopology opts in to use MongoDB driver's connection management engine, otherwise is false by defualt.*/
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost/medTracker_db", {
     userNewUrlParse: true,
     useUnifiedTopology: true
});

/*Creates variables for the app and listening port.*/
const app = express();
const PORT = 3250;

/*At root directory responds with the defined message. This is handleing a GET request to the server.*/
app.get("/", (req, res) => {
     res.json({
          message: "MedTracker API v1"
     });
});

/*This starts the applicaiton and prints the message defined to the Terminal console.*/
app.listen(PORT, () => {
     console.log(`Express server is started and listening on PORT ${PORT}. To stop this server <Ctrl>+C`);
});
